/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <locale.h>
#include <gcrypt.h>
#include "debug.h"
#include "app.h"
#include "repl.h"
#include "util.h"

#define PASSMAN_GCRYPT_SECMEM_SIZE (32 * 1024)

void init_security();
void clean_security();

int main(int argc, char **argv) {
  wchar_t *dbname = NULL;

  fwide(stdin, 1);
  fwide(stdout, 1);

  if(!setlocale(LC_ALL, ""))
    warning(L"Unable to set locale; data corruption might arise");

  #ifdef DEBUG /* Debug banner */
    fputws(L"Debug mode ENABLED\n", stdout);
    wprintf(L"%ls\n", APP_VERSION);
  #endif

  init_security();

  if(argc > 1)
    dbname = from_multibyte(argv[1]);

  repl_run(dbname);

  return 0;
}

void init_security() {
  /* Lock memory */
  #ifndef DEBUG
    if(mlockall(MCL_FUTURE) == -1) {
      warning(L"Unable to lock memory");
    }
  #endif /* DEBUG */

  /* Initialize gcrypt */
  if(!gcry_check_version(GCRYPT_VERSION))
    fatal(L"Invalid gcrypt version");

  gcry_control(GCRYCTL_SUSPEND_SECMEM_WARN);
  gcry_control(GCRYCTL_INIT_SECMEM, PASSMAN_GCRYPT_SECMEM_SIZE, 0);
  gcry_control(GCRYCTL_RESUME_SECMEM_WARN);
  gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
}

void clean_security() {
  /* Unlock memory */
  #ifndef DEBUG
    munlockall();
  #endif /* DEBUG */
}
