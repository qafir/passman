/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef _APP_H
#define _APP_H

#define _PASSMAN_STRINGIFY(s) L ## #s
#define PASSMAN_STRINGIFY(s) _PASSMAN_STRINGIFY(s)

#define APP_NAME L"passman"
#define APP_VERSION_MAJOR 1
#define APP_VERSION_MINOR 1
#define APP_VERSION_PATCH 0
#define APP_VERSION_PHASE L"beta"
#define _APP_VERSION(NAME, MAJOR, MINOR, PATCH, PHASE) (NAME L" v" MAJOR L"." MINOR L"." PATCH L" " PHASE)
#define APP_VERSION \
  _APP_VERSION( \
    APP_NAME, \
    PASSMAN_STRINGIFY(APP_VERSION_MAJOR), \
    PASSMAN_STRINGIFY(APP_VERSION_MINOR), \
    PASSMAN_STRINGIFY(APP_VERSION_PATCH), \
    APP_VERSION_PHASE)

#endif /* _APP_H */
