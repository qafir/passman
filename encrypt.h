/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef _ENCRYPT_H
#define _ENCRYPT_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/*
  Authentication tag size in bytes.
*/
#define ENCRYPT_AUTHTAG_SIZE 16

/*
  Cipher key size in bytes.
*/
#define ENCRYPT_CIPHER_KEYSIZE 32

/*
  Encryption block size in bytes.
*/
#define ENCRYPT_BLOCKSIZE 16

/*
  Key salt size in bytes.
*/
#define ENCRYPT_SALTSIZE 128

/*
  Hashed key size in bytes.
*/
#define ENCRYPT_HASHED_KEYSIZE 256

/*
  Key hashing iterations.
*/
#define ENCRYPT_KEYHASH_ITERATIONS 1024

/*
  Encrypt/Decrypt data with AES256-CTR.

  ARGUMENTS
    key_sz: The key size
    key: The plaintext key as a null-terminated string
    salt: Salt for key hashing
    ctr: The IV (must be ENCRYPT_BLOCKSIZE long)
    sz: Input and output buffer size in bytes (must be an ENCRYPT_BLOCKSIZE integer multiple)
    in: The input data
    out: The output buffer

*/
void crypt(size_t key_sz, const uint8_t *key, const uint8_t *salt, const uint8_t *ctr, size_t sz, const uint8_t *in, uint8_t *out, uint32_t iterations);

/*
  Computes a hashed key by using PBKDF2-HMAC-SHA256.

  ARGUMENTS
    keysz: The key size in bytes
    key: The plaintext key
    salt: Salt of size ENCRYPT_SALTSIZE bytes
    outlen: Length of output buffer
    out: Output buffer
    iterations: number of hashing iterations

  RETURN VALUE
    A hashed key of length `outlen`
*/
void encrypt_derive_key(size_t key_sz, const void *key, const void *salt, size_t outlen, uint8_t *out, uint32_t iterations);

/*
  Encrypts data for file storage and saves it.

  ARGUMENTS
    path: Output file path
    iterations: Number of key hashing iterations
    key: Plaintext encryption key
    len: `data` size in bytes
    data: Pointer to the data to encrypt and save to file

  RETURN VALUE
    True on success, false on failure. `errno` is also set accordingly upon return

  NOTES
    This function assumes the random number generation module has been initialized.
*/
bool encrypt_to_file(const wchar_t *path, uint32_t iterations, const wchar_t *key, size_t len, uint8_t *data);

/*
  Decrypts data from file storage after loading it.

  ARGUMENTS
    path: Input file path
    key: Plaintext encryption key
    iterations: Pointer to memory location where the number of key hashing iterations will be stored
    size: Pointer to memory location where the size of the returned data will be stored

  RETURN VALUE
    A pointer to the decrypted data on success; NULL on failure. `errno` is
    also set accordingly upon return.  The values of `iterations` and `size`
    are unspecified upon failure. The application must free the returned
    pointer when finished with the data. If an empty database is read, NULL is
    returned and `*size` is set to SIZE_MAX
*/
uint8_t *decrypt_from_file(const wchar_t *path, const wchar_t *key, uint32_t *iterations, size_t *size);

#endif /* _ENCRYPT_H */
