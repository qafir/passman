/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "util.h"
#include "debug.h"

/*
  Print a message to STDERR.

  ARGUMENTS
    msg: The message
    prefix: A prefix for the message
*/
void printerr(const wchar_t *msg, const wchar_t *prefix) {
  fwprintf(stdout, L"%ls: %ls\n", prefix, msg);
}

void fatal(const wchar_t *msg) {
  printerr(msg, L"FATAL");
  abort();
}

void mbfatal(const char *msg) {
  fatal(from_multibyte(msg));
}

void error(const wchar_t *msg) {
  printerr(msg, L"ERROR");
}

void warning(const wchar_t *msg) {
  printerr(msg, L"WARNING");
}

void error_errno() {
#define error_errno() error(strerror(errno))
  wchar_t *errstr = from_multibyte(strerror(errno));

  error(errstr);
  free(errstr);
}
