/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef _UTIL_H
#define _UTIL_H

#include <stdbool.h>

/*
  Tells if a file exists and is readable.

  ARGUMENTS
    path: The file path

  RETURN VALUE
    True if the file exists and is readable by the current user, false if not.
    `errno` is set accordingly
*/
bool fexists(const wchar_t *path);

/*
  Converts a string from wide to multibyte.

  ARGUMENTS
    ws: The wide-character string

  RETURN VALUE
    A pointer to a multibyte representation of the given wide-character string.
    The application is responsible for freeing the returned pointer

  NOTES
    If cannot convert every character, an abort() is performed
*/
char *to_multibyte(const wchar_t *ws);

/*
  Converts a string from multibyte to wide.

  ARGUMENTS
    s: The multibyte string

  RETURN VALUE
    A pointer to a wide representation of the given multibyte string.
    The application is responsible for freeing the returned pointer

  NOTES
    If cannot convert every character, an abort() is performed
*/
wchar_t *from_multibyte(const char *s);

/*
  Converts a string from wide to UTF-8.

  ARGUMENTS
    ws: The wide-character string

  RETURN VALUE
    A pointer to a UTF-8 representation of the given wide-character string.
    The application is responsible for freeing the returned pointer

  NOTES
    If cannot convert every character, an abort() is performed
*/
char *to_utf8(const wchar_t *ws);

/*
  Converts a string from UTF-8 to wide.

  ARGUMENTS
    s: The UTF-8 string

  RETURN VALUE
    A pointer to a wide representation of the given UTF-8 string.
    The application is responsible for freeing the returned pointer

  NOTES
    If cannot convert every character, an abort() is performed
*/
wchar_t *from_utf8(const char *s);

/*
  Converts a string to lowercase.

  ARGUMENTS
    in: The input string (must be NULL terminated)

  RETURN VALUE
    A new wide string containing the lowercase input text of the first string.
    The returned value must be manually freed.
*/
wchar_t *ws_tolower(const wchar_t *in);

/*
  Case insensitive wide character substring search.

  ARGUMENTS
    in: The string to search in
    what: The string to search for

  RETURN VALUE
    True if `what` is found inside `in`, false if not.
*/
bool ws_isearch(const wchar_t *in, const wchar_t* what);

/*
  Performs tilde expansion.

  ARGUMENTS
    path: A path to expand

  RETURN VALUE
    A newly allocated string containing the expanded path. The caller is
    responsible for freeing the returned memory. If there is no expansion
    to perform, no memory is allocated and `path` is returned. If an error
    occurs, no memory is allocated and NULL is returned.
*/
wchar_t *expand_tilde(const wchar_t *path);

#endif /* _UTIL_H */
