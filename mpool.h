/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef _MPOOL_H
#define _MPOOL_H

#include <stdint.h>
#include <stddef.h>
#include "helper.h"

#define MPOOL_INIT_SIZE kibibytes(64)
#define MPOOL_INCR_SIZE kibibytes(64)

/*
  Memory pool.
*/
typedef struct TAG_MPOOL {
  void *base; /* Base pointer */
  size_t size /* Current pool used size, in bytes */, capacity /* Total pool capacity, in bytes */;
} MPOOL;

/*
  Initialize a memory pool instance.

  The initialized object must be freed by mpool_free().
*/
void mpool_init(MPOOL *pool);

/*
  Free a memory pool instance.

  The `pool` pointer is not freed but all its internal allocated memory is.
*/
void mpool_free(MPOOL *pool);

/*
  Ensure there is enough space in the memory pool for an object of given size.
  If there isn't enough space, the memory pool is expanded as needed.

  ARGUMENTS
    pool: The memory pool
    sz: The size to ensure is available in the pool after this function returns
*/
void mpool_ensure_size(MPOOL *pool, size_t sz);

#define FUNC(type, name) void mpool_write_##name(MPOOL *pool, type value)
UNUSED FUNC(uint8_t, u8);
#undef FUNC

/*
  Write an unsigned integer of size `sz`.

  ARGUMENTS
    value: The integer to write to the pool
    pool: The pool instance
*/
#define FUNC(sz) void mpool_write_u##sz(MPOOL *pool, uint##sz##_t value)
UNUSED FUNC(16);
UNUSED FUNC(32);
UNUSED FUNC(64);
#undef FUNC

/*
  Write an UTF-8 encoded string.

  ARGUMENTS
    pool: The pool instance
    value: An UTF-8 encoded, NULL terminated string
*/
UNUSED void mpool_write_utf8(MPOOL *pool, const char *value);

/*
  Write a block of data.

  ARGUMENTS
    pool: The pool instance
    block: The block of data
    size: The size of the block
*/
UNUSED void mpool_write_block(MPOOL *pool, const uint8_t *block, size_t size);

#endif /* _MPOOL_H */
