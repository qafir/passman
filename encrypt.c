/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#define GCRYPT_NO_DEPRECATED
#include <stdio.h>
#include <stdlib.h>
#include <endian.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <stdint.h>
#include <assert.h>
#include <gcrypt.h>
#include "debug.h"
#include "encrypt.h"
#include "scramble.h"
#include "util.h"

/*
  Expands to a secure gcrypt call. If the call to action `x` fails, a message
  describing the error is displayed and the program is aborted.

  ARGUMENTS
    x: An expression resulting in a gcrypt_error_t value
*/
#define EGCRY_CALL(x) do { gcry_error_t _err = (x); if(_err) { mbfatal(gcry_strerror(_err)); } } while(0)

void crypt(size_t key_sz, const uint8_t *key, const uint8_t *salt, const uint8_t *ctr, size_t sz, const uint8_t *in, uint8_t *out, uint32_t iterations) {
  uint8_t hkey[ENCRYPT_CIPHER_KEYSIZE]; /* Hashed key */
  gcry_cipher_hd_t cipher;

  EGCRY_CALL(gcry_cipher_open(&cipher, GCRY_CIPHER_TWOFISH, GCRY_CIPHER_MODE_CTR, GCRY_CIPHER_SECURE));

  /* Hash & set key */
  encrypt_derive_key(key_sz, key, salt, ENCRYPT_CIPHER_KEYSIZE, hkey, iterations);
  EGCRY_CALL(gcry_cipher_setkey(cipher, hkey, ENCRYPT_CIPHER_KEYSIZE));

  /* Set CTR */
  EGCRY_CALL(gcry_cipher_setctr(cipher, ctr, ENCRYPT_BLOCKSIZE));

  /* Encrypt */
  EGCRY_CALL(gcry_cipher_encrypt(cipher, out, sz, (const unsigned char *)in, sz));

  /* Cleanup */
  scramble(ENCRYPT_CIPHER_KEYSIZE, hkey, SCRAMBLE_DEFAULT_ITERATIONS);
  gcry_cipher_close(cipher);
}

void encrypt_derive_key(size_t keylen, const void *key, const void *salt, size_t outlen, uint8_t *out, uint32_t iterations) {
  EGCRY_CALL(gcry_kdf_derive(key, keylen, GCRY_KDF_PBKDF2, GCRY_MD_WHIRLPOOL, salt, ENCRYPT_SALTSIZE, (unsigned long)iterations, outlen, out));
}

bool encrypt_to_file(const wchar_t *path, uint32_t iterations, const wchar_t *key, size_t len, uint8_t *data) {
  gcry_cipher_hd_t cipher;
  uint8_t iv[ENCRYPT_BLOCKSIZE];
  uint8_t *out;
  FILE *f = NULL;
  bool fresult = true; /* File operation result: true is good, false is not that good */
  uint8_t salt[ENCRYPT_SALTSIZE];
  uint8_t hkey[ENCRYPT_CIPHER_KEYSIZE];
  uint8_t tag[ENCRYPT_AUTHTAG_SIZE];
  char *spath = to_multibyte(path);
  char *skey = to_utf8(key);
  uint32_t iterations_be = htobe32(iterations);

  /* Initialize */
  errno = 0;

  gcry_create_nonce(iv, ENCRYPT_BLOCKSIZE);
  gcry_create_nonce(salt, ENCRYPT_SALTSIZE);
  encrypt_derive_key(strlen(skey) * sizeof(char), (const uint8_t *)skey, salt, ENCRYPT_CIPHER_KEYSIZE, hkey, iterations);
  EGCRY_CALL(gcry_cipher_open(&cipher, GCRY_CIPHER_TWOFISH, GCRY_CIPHER_MODE_GCM, GCRY_CIPHER_SECURE));
  EGCRY_CALL(gcry_cipher_setkey(cipher, hkey, ENCRYPT_CIPHER_KEYSIZE));
  EGCRY_CALL(gcry_cipher_setctr(cipher, iv, ENCRYPT_BLOCKSIZE));

  /* Initialize buffers */
  out = calloc(len, sizeof(uint8_t));

  /* Encrypt data & get tag */
  EGCRY_CALL(gcry_cipher_encrypt(cipher, out, len, data, len));
  EGCRY_CALL(gcry_cipher_gettag(cipher, tag, ENCRYPT_AUTHTAG_SIZE));

  /* Save data to file */
  if((f = fopen(spath, "wb")) != NULL) {
    if(
      fwrite(iv, ENCRYPT_BLOCKSIZE, 1, f) != 1 /* Write original counter */
      || fwrite(salt, ENCRYPT_SALTSIZE, 1, f) != 1 /* Write salt */
      || fwrite(tag, ENCRYPT_AUTHTAG_SIZE, 1, f) != 1 /* Write authentication tag */
      || fwrite(&iterations_be, sizeof(uint32_t), 1, f) != 1 /* Write iterations */
    ) /* Didn't write everything */
      fresult = false;

    if(len != 0) { /* Database not empty */
      if(fwrite(out, len, 1, f) != 1)
        fresult = false; /* There is data but not everything has been written. Bad news! */
    }

    fclose(f);
  } else /* Oops, can't open file! */
    fresult = false;

  /* Cleanup */
  gcry_cipher_close(cipher);
  free(out);
  scramble(sizeof(ENCRYPT_CIPHER_KEYSIZE), hkey, SCRAMBLE_DEFAULT_ITERATIONS);
  sfree(strlen(skey) * sizeof(char), skey);
  sfree(strlen(spath) * sizeof(char), spath);

  return fresult;
}

uint8_t *decrypt_from_file(const wchar_t *path, const wchar_t *key, uint32_t *iterations, size_t *size) {
  gcry_cipher_hd_t cipher;
  uint8_t iv[ENCRYPT_BLOCKSIZE];
  FILE *f;
  bool fresult = true; /* Same as in ecnrypt_to_file() */
  uint8_t *data;
  uint8_t salt[ENCRYPT_SALTSIZE];
  uint8_t hkey[ENCRYPT_CIPHER_KEYSIZE];
  uint8_t tag[ENCRYPT_AUTHTAG_SIZE];
  char *spath = to_multibyte(path);
  char *skey = to_utf8(key);
  
  /* Initialize */
  errno = 0;
  EGCRY_CALL(gcry_cipher_open(&cipher, GCRY_CIPHER_TWOFISH, GCRY_CIPHER_MODE_GCM, GCRY_CIPHER_SECURE));

  /* Read data */
  if((f = fopen(spath, "rb")) != NULL) {
    if(
      fread(iv, ENCRYPT_BLOCKSIZE, 1, f) != 1
      || fread(salt, ENCRYPT_SALTSIZE, 1, f) != 1
      || fread(tag, ENCRYPT_AUTHTAG_SIZE, 1, f) != 1
      || fread(iterations, sizeof(uint32_t), 1, f) != 1
    )
      fresult = false;
    else { /* Header read correctly */
      long base = ftell(f);
      long end;

      fseek(f, 0, SEEK_END);
      end = ftell(f);
      fseek(f, base, SEEK_SET);

      *iterations = be32toh(*iterations);
      *size = end - base;

      if(*size != 0) {
        data = malloc(*size);

        if(fread(data, *size, 1, f) == 1) {
          /* Initialize decryption key */
          encrypt_derive_key(strlen(skey) * sizeof(char), (const uint8_t *)skey, salt, ENCRYPT_CIPHER_KEYSIZE, hkey, *iterations);

          /* Decrypt data */
          EGCRY_CALL(gcry_cipher_setkey(cipher, hkey, ENCRYPT_CIPHER_KEYSIZE));
          EGCRY_CALL(gcry_cipher_decrypt(cipher, data, *size, data, *size));

          /* Verify authentication tag */
          { /* TODO: Allow user to forcefully skip auth step */
            gcry_error_t err = gcry_cipher_checktag(cipher, tag, ENCRYPT_AUTHTAG_SIZE);
            
            if(gcry_err_code(err) == GPG_ERR_CHECKSUM) {
              fresult = false;
              error(L"Data authentiaction failed. Data is corrupt.");
            } else EGCRY_CALL(err);
          }
        } else /* Couldn't read data */
          fresult = false;

        if(!fresult)
          free(data);
      } else {
        data = NULL;
        *size = SIZE_MAX;
      }
    }

    fclose(f);
  } else fresult = false;

  gcry_cipher_close(cipher);
  scramble(sizeof(ENCRYPT_CIPHER_KEYSIZE), &hkey, SCRAMBLE_DEFAULT_ITERATIONS);
  sfree(strlen(skey) * sizeof(char), skey);
  sfree(strlen(spath) * sizeof(char), spath);

  return fresult? data: NULL;
}
