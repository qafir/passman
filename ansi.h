/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef _ANSI_H
#define _ANSI_H

#define _ANSI_STRINGIFY(s) L ## #s
#define ANSI_STRINGIFY(s) _ANSI_STRINGIFY(s)

/* Escape sequence */
#define ANSI_ESC L"\x1b["

/* Set position */
#define ANSI_CUR_POS(row, col) ANSI_ESC ANSI_STRINGIFY(row) L";" ANSI_STRINGIFY(col) L"H"

/* Move cursor `n` positions up, down, forward or backward */
#define ANSI_CUR_UP(n) ANSI_ESC ANSI_STRINGIFY(n) L"A"
#define ANSI_CUR_DN(n) ANSI_ESC ANSI_STRINGIFY(n) L"B"
#define ANSI_CUR_FW(n) ANSI_ESC ANSI_STRINGIFY(n) L"C"
#define ANSI_CUR_BK(n) ANSI_ESC ANSI_STRINGIFY(n) L"D"

/* Save or restore cursor position */
#define ANSI_CUR_SAVE ANSI_ESC L"s"
#define ANSI_CUR_RESTORE ANSI_ESC L"u"

/* Erase entire display or current line */
#define ANSI_ERASE_DISPLAY ANSI_ESC L"2J"
#define ANSI_ERASE_LINE ANSI_ESC L"K"

/* Set attributes. Usage: ANSI_ATTR(ATTR1 ATTR2 ATTR3 ...) */
#define ANSI_ATTR(attrs) ANSI_ESC attrs L"m"

/* Set and reset display mode */
#define ANSI_MODE_SET(mode) ANSI_ESC L"=" mode L"h"
#define ANSI_MODE_RESET(mode) ANSI_ESC L"=" mode L"l"

/* ANSI attributes */
#define ANSI_ATTR_DEFAULT L"0"
#define ANSI_ATTR_BOLD L"1"
#define ANSI_ATTR_FAINT L"2"
#define ANSI_ATTR_STANDOUT L"3"
#define ANSI_ATTR_UNDERLINE L"4"
#define ANSI_ATTR_BLINK L"5"
#define ANSI_ATTR_REVERSE L"7"
#define ANSI_ATTR_INVISIBLE L"8"
#define ANSI_ATTR_NORMAL L"22"
#define ANSI_ATTR_STANDOUT_OFF L"23"
#define ANSI_ATTR_UNDERLINE_OFF L"24"
#define ANSI_ATTR_BLINK_OFF L"25"
#define ANSI_ATTR_REVERSE_OFF L"27"
#define ANSI_ATTR_FG_BLACK L"30"
#define ANSI_ATTR_FG_RED L"31"
#define ANSI_ATTR_FG_GREEN L"32"
#define ANSI_ATTR_FG_YELLOW L"33"
#define ANSI_ATTR_FG_BLUE L"34"
#define ANSI_ATTR_FG_MAGENTA L"35"
#define ANSI_ATTR_FG_CYAN L"36"
#define ANSI_ATTR_FG_WHITE L"37"
#define ANSI_ATTR_FG_DEFAULT L"39"
#define ANSI_ATTR_BG_BLACK L"40"
#define ANSI_ATTR_BG_RED L"41"
#define ANSI_ATTR_BG_GREEN L"42"
#define ANSI_ATTR_BG_YELLOW L"43"
#define ANSI_ATTR_BG_BLUE L"44"
#define ANSI_ATTR_BG_MAGENTA L"45"
#define ANSI_ATTR_BG_CYAN L"46"
#define ANSI_ATTR_BG_WHITE L"47"
#define ANSI_ATTR_BG_DEFAULT L"49"

/*
  ANSI modes

  Modes are listed as ANSI_MODE_CCRRK, where:
    - CC: # width
    - RR: # height
    -  K: color mode
            * M: monochrome
            * N: color
            * F: 4 colors
            * C: 16 colors
            * H: 256 colors

  K is always one character long; CCRR can be 4 digits or 6 digits long.

  If CCRR is 4 digits long, then the first two are the number of text
  columns and the second two are the number of text rows;
  if CCRR is 6 digits long, then the first three are the number of pixels
  in a row and the second three are the total number of rows.

  Example:
    ANSI_MODE_4025MT means a 40x25 monochrome text display mode.

*/

#define ANSI_MODE_4025M L"0"
#define ANSI_MODE_4025N L"1"
#define ANSI_MODE_8025M L"2"
#define ANSI_MODE_8025N L"3"

#define ANSI_MODE_320200F L"4"
#define ANSI_MODE_320200M L"5"
#define ANSI_MODE_640200M L"6"
#define ANSI_MODE_320200N L"13"
#define ANSI_MODE_640200C L"14"
#define ANSI_MODE_640350M L"15"
#define ANSI_MODE_640350C L"16"
#define ANSI_MODE_640480M L"17"
#define ANSI_MODE_640480C L"18"
#define ANSI_MODE_320200H L"19"

#define ANSI_MODE_WRAP L"7" /* Enable text wrapping */

#endif /* _ANSI_H */
