SHELL=/bin/bash
LIBS=-lm -lgcrypt
CFLAGS+=-std=c11 -Wall -D_DEFAULT_SOURCE -Iqcollect/install/include
LDFLAGS+=-Wall $(LIBS)
MAIN=main
SOURCES=$(MAIN).c debug.c encrypt.c scramble.c pdb.c util.c repl.c mpool.c
BINNAME=passman
BINFILE=$(BINNAME)
DEBUG=0
LIBQCOLLECT=qcollect/install/lib/libqcollect.a
PREFIX=usr/local
BINDIR=$(PREFIX)/bin
DESTDIR=

OBJECTS=$(foreach src,$(SOURCES),$(basename $(src)).o)

export DEBUG

ifeq ($(DEBUG),1)
CFLAGS+=-DDEBUG -ggdb -Werror
LDFLAGS+=-Werror
else
CFLAGS+=-g0 -O2
endif

.PHONY: clean distclean test

all: $(BINFILE)

$(BINFILE): $(LIBQCOLLECT) $(OBJECTS)
	gcc $(LDFLAGS) -o $(BINFILE) $(OBJECTS) $(LIBQCOLLECT)

$(OBJECTS): %.o: %.c
	gcc -c $(CFLAGS) -o $@ $<

$(LIBQCOLLECT):
	$(MAKE) -C qcollect
	$(MAKE) -C qcollect DESTDIR=install install

clean: distclean
	$(RM) $(BINFILE)

distclean:
	$(RM) $(OBJECTS) $(TESTOBJS) test/unittest

test/unittest: OBJECTS := $(filter-out $(MAIN).o,$(OBJECTS))
test/unittest: %: %.c $(OBJECTS) 
	gcc $(CFLAGS) $(LDFLAGS) -o test/unittest $(OBJECTS) test/unittest.c $(LIBQCOLLECT)

test: test/unittest
	test/unittest

install: DBINDIR=$(DESTDIR)/$(BINDIR)
install: DBINFILE=$(DBINDIR)/$(BINFILE)
install: $(BINFILE)
	install -m755 -d $(DBINDIR)
	install -m0755 $(BINFILE) $(DBINDIR)/
	setcap cap_ipc_lock+ep $(DBINFILE)
