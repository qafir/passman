/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <endian.h>
#include <wchar.h>
#include <gcrypt.h>
#include "mpool.h"
#include "scramble.h"
#include "pdb.h"
#include "util.h"

static void free_credential(PDB_CREDENTIAL *c) {
  /* Free credential base data */
  sfree(c->title.size, c->title.ptr);
  sfree(c->username.size, c->username.ptr);
  sfree(c->password.size, c->password.ptr);
  sfree(c->url.size, c->url.ptr);
  sfree(c->tags.size, c->tags.ptr);

  /* Free credential associated data */
  for(size_t j = 0; j < c->data->length; j++) {
    PDB_DATA *d = c->data->arr[j]->val.ptr;

    pdb_data_clear_content(d);
    sfree(sizeof(PDB_DATA), d);
  }

  free(c->data->arr);
  free(c->data);
  sfree(sizeof(PDB_CREDENTIAL), c);
}

void pdb_data_clear_content(PDB_DATA *d) {
  if(d->value)
    sfree(d->size, d->value);

  sfree(d->name.size, d->name.ptr);

  d->value = NULL;
  d->size = 0;
}

wchar_t *pdb_get_textentry_value(const PDB_DATABASE *db, const PDB_TEXTENTRY *t, bool decrypt) {
  wchar_t *result;

  if(t->size > 0) {
    result = malloc(t->size);

  if(decrypt)
    pdb_crypt(db, t->size, (const uint8_t *)t->ptr, (uint8_t *)result);
  else
    wcscpy(result, t->ptr);
  } else
    result = calloc(1, sizeof(wchar_t));

  return result;
}

PDB_TEXTENTRY *pdb_set_textentry_value(const PDB_DATABASE *db, PDB_TEXTENTRY *t, const wchar_t *value, bool encrypt) {
  sfree(t->size, t->ptr);

  t->size = (wcslen(value) + 1) * sizeof(wchar_t);
  t->ptr = malloc(t->size);
  
  if(encrypt)
    pdb_crypt(db, t->size, (const uint8_t *)value, (uint8_t *)t->ptr);
  else
    wcscpy(t->ptr, value);

  return t;
}

PDB_DATA* pdb_new_data(const PDB_DATABASE *db, const PDB_CREDENTIAL *c, const wchar_t *name, PDB_DATA_TYPE type) {
  if(!pdb_get_data(c, name)) {
    PDB_DATA *d = calloc(1, sizeof(PDB_DATA));
    QC_VALUE val = { .ptr = d };

    pdb_set_textentry_value(db, &d->name, name, false);

    d->type = type;

    qc_al_append(c->data, val);

    return d;
  } else
    return NULL; /* Already exists */
}

PDB_DATA* pdb_get_data(const PDB_CREDENTIAL *c, const wchar_t *name) {
  size_t nlen = wcslen(name);

  for(size_t i = 0; i < c->data->length; i++) {
    PDB_DATA *d = c->data->arr[i]->val.ptr;

    if(wcsncmp(d->name.ptr, name, nlen) == 0) /* Match found */
      return d;
  }

  return NULL; /* Nothing found if we are here */
}

PDB_CREDENTIAL* pdb_get_credential(const PDB_DATABASE *db, const wchar_t *title) {
  size_t tlen = wcslen(title);

  for(size_t i = 0; i < db->ecreds->length; i++) {
    PDB_CREDENTIAL *c = db->ecreds->arr[i]->val.ptr;

    if(wcsncmp(title, c->title.ptr, tlen) == 0) /* Match */
        return c;
  }

  return NULL; /* Nothing found if we are here */
}

PDB_CREDENTIAL* pdb_new_credential(const PDB_DATABASE *db, const wchar_t *title) {
  PDB_CREDENTIAL *c;

  if(!pdb_get_credential(db, title)) {
    size_t title_sz = (wcslen(title) + 1) * sizeof(wchar_t);
    QC_VALUE al_val;

    c = calloc(1, sizeof(PDB_CREDENTIAL));
    
    c->title.size = title_sz;
    c->title.ptr = malloc(title_sz);
    wcscpy(c->title.ptr, title);

    c->data = qc_al_init();

    al_val.ptr = c;
    qc_al_append(db->ecreds, al_val);

    pdb_set_credential_username(db, c, L"");
    pdb_set_credential_password(db, c, L"");
    pdb_set_credential_url(db, c, L"");
    pdb_set_credential_tags(db, c, L"");

    return c;
  } else
    return NULL; /* A new credential is already present */
}

void pdb_set_key(PDB_DATABASE *db, const wchar_t *key) {
  size_t keysz = wcslen(key) * sizeof(wchar_t);

  /* Clean up old key */
  if(db->keysz)
    sfree(db->keysz, db->key);

  /* Encrypt key */
  db->key = malloc(keysz);
  db->keysz = keysz;
  pdb_crypt(db, keysz, (const uint8_t *)key, db->key);
}

PDB_DATABASE* pdb_new(const wchar_t *path, const wchar_t *key) {
  PDB_DATABASE *db;
  size_t pathlen = (wcslen(path) + 1);
  size_t pathsz = pathlen * sizeof(wchar_t);

  /* Initialize memory */
  db = calloc(1, sizeof(PDB_DATABASE));

  /* Initialize metadata */
  db->path = malloc(pathsz);
  wcsncpy(db->path, path, pathlen);

  gcry_create_nonce(db->ctr, ENCRYPT_BLOCKSIZE);
  gcry_create_nonce(db->salt, ENCRYPT_SALTSIZE);
  gcry_randomize(db->memkey, PDB_MEMKEY_SIZE, GCRY_STRONG_RANDOM);
  
  db->iterations = ENCRYPT_KEYHASH_ITERATIONS;
  db->ecreds = qc_al_init();

  /* Set key */
  pdb_set_key(db, key);

  return db;
}

void pdb_crypt(PDB_DATABASE const *db, size_t sz, uint8_t const *data, uint8_t *out) {
  crypt(PDB_MEMKEY_SIZE, db->memkey, db->salt, db->ctr, sz, data, out, db->iterations);
}

/*
  Decodes a text entry from raw data.

  ARGUMENTS
    db: Database pointer
    ptr: Raw data pointer
    e: Output pointer
    encrypt: True to encrypt decoded data

  RETURN VALUE
    Number of decoded bytes
*/
static size_t pdb_decode_textentry(const PDB_DATABASE *db, uint8_t *ptr, PDB_TEXTENTRY *e, bool encrypt) {
  size_t sz = be16toh(*((uint16_t *)ptr));
  char *s = malloc(sz + sizeof(char)); /* String size + one NULL-termination char */

  memcpy(s, ptr + sizeof(uint16_t), sz);
  s[sz] = '\0';

  e->ptr = from_utf8(s);
  e->size = (1 + wcslen(e->ptr)) * sizeof(wchar_t);

  if(encrypt) {
    pdb_crypt(db, e->size, (const uint8_t *)e->ptr, (uint8_t *)e->ptr);
  }

  free(s);

  return sz + sizeof(uint16_t);
}

/*
  Decodes a PDB_DATA from raw data.

  ARGUMENTS
    db: Database pointer
    ptr: Raw data pointer
    d: Output pointer

  RETURN VALUE
    Number of decoded bytes
*/
static size_t pdb_decode_credential_data(const PDB_DATABASE *db, uint8_t *ptr, PDB_DATA *d) {
  size_t offset = 0;

  offset += pdb_decode_textentry(db, ptr, &d->name, false);
  
  d->type = be16toh(*((uint16_t *)(ptr + offset))); offset += sizeof(uint16_t);
  d->size = be64toh(*((uint64_t *)(ptr + offset))); offset += sizeof(uint64_t);
  d->value = malloc(d->size);

  memcpy(d->value, ptr + offset, d->size); offset += d->size;
  pdb_crypt(db, d->size, d->value, d->value);

  return offset;
}

/*
  Decodes a PDB_CREDENTIAL from raw data.

  ARGUMENTS
    db: Database pointer
    ptr: Raw data pointer
    c: Output pointer

  RETURN VALUE
    Number of decoded bytes
*/
static size_t pdb_decode_credential(const PDB_DATABASE *db, uint8_t *ptr, PDB_CREDENTIAL *c) {
  size_t offset = 0;

  /* Basic data */
  offset += pdb_decode_textentry(db, ptr, &c->title, false);
  offset += pdb_decode_textentry(db, ptr + offset, &c->username, true);
  offset += pdb_decode_textentry(db, ptr + offset, &c->password, true);
  offset += pdb_decode_textentry(db, ptr + offset, &c->url, false);
  offset += pdb_decode_textentry(db, ptr + offset, &c->tags, false);

  /* Associated data */
  size_t data_elements = be16toh(*((uint16_t *)(ptr + offset))); offset += sizeof(uint16_t);

  for(size_t i = 0; i < data_elements; i++) {
    PDB_DATA *d = malloc(sizeof(PDB_DATA));
    union QCOLLECT_VALUE l_val;

    offset += pdb_decode_credential_data(db, ptr + offset, d);
    l_val.ptr = d;
    qc_al_append(c->data, l_val);
  }

  return offset;
}

PDB_DATABASE *pdb_load(const wchar_t *path, const wchar_t *key) {
  if(fexists(path)) {
    PDB_DATABASE *db = pdb_new(path, key);
    size_t payload_sz = 0;
    uint8_t *payload = decrypt_from_file(path, key, &db->iterations, &payload_sz);

    if(payload) { /* Data decrypted */
      uint8_t *ptr = payload, *end = payload + payload_sz;

      while(ptr < end) { /* Iterate through raw data */
        /* Decode credential */
        PDB_CREDENTIAL *c = calloc(1, sizeof(PDB_CREDENTIAL));
        QC_VALUE n = { .ptr = c };

        c->data = qc_al_init();
        ptr += pdb_decode_credential(db, ptr, c);

        qc_al_append(db->ecreds, n);
      }

      sfree(payload_sz, payload);
      return db;
    } else if(payload_sz == SIZE_MAX) { /* Empty database */
      return db;
    } else { /* Failed to decrypt data */
      pdb_free(db);
      return NULL;
    }
  } else
    return NULL; /* File does not exist */
}

/*
  Encodes a PDB_TEXTENTRY to raw data.

  ARGUMENTS
    db: Database pointer
    in: Input data pointer
    pool: Memory pool instance
    decrypt: True to decrypt content before encoding
*/
static void pdb_encode_textentry(const PDB_DATABASE *db, const PDB_TEXTENTRY *in, MPOOL *pool, bool decrypt) {
  wchar_t *wc;
  char *sin;
  size_t sin_sz;

  if(decrypt) {
    wc = malloc(in->size);
    pdb_crypt(db, in->size, (const uint8_t *)in->ptr, (uint8_t *)wc);
  } else
    wc = in->ptr;

  sin = to_utf8(wc);
  sin_sz = strlen(sin) * sizeof(char);

  mpool_write_utf8(pool, sin);
  sfree(sin_sz, sin);

  if(decrypt)
    sfree(in->size, wc);
}

/*
  Encodes a PDB_DATA to raw data.

  ARGUMENTS
    db: Database pointer
    in: Input data pointer
    pool: Memory pool
*/
static void pdb_encode_credential_data(const PDB_DATABASE *db, const PDB_DATA *in, MPOOL *pool) {
  /* Name */
  pdb_encode_textentry(db, &in->name, pool, false);

  /* Type */
  mpool_write_u16(pool, (uint16_t)in->type);

  /* Size & value */
  mpool_write_u64(pool, in->size);

  pdb_crypt(db, in->size, in->value, in->value);
  mpool_write_block(pool, in->value, in->size);
  pdb_crypt(db, in->size, in->value, in->value);
}

/*
  Encodes a PDB_CREDENTIAL to raw data.

  ARGUMENTS
    db: Database pointer
    in: Input data pointer
    pool: Memory pool
*/
static void pdb_encode_credential(const PDB_DATABASE *db, const PDB_CREDENTIAL *in, MPOOL *pool) {
  pdb_encode_textentry(db, &in->title, pool, false);
  pdb_encode_textentry(db, &in->username, pool, true);
  pdb_encode_textentry(db, &in->password, pool, true);
  pdb_encode_textentry(db, &in->url, pool, false);
  pdb_encode_textentry(db, &in->tags, pool, false);

  /* Credential data */
  mpool_write_u16(pool, in->data->length);

  for(size_t j = 0; j < in->data->length; j++)
    pdb_encode_credential_data(db, in->data->arr[j]->val.ptr, pool);
}

bool pdb_save(PDB_DATABASE const *db) {
  MPOOL pool;

  /* Populate payload */
  mpool_init(&pool);

  for(size_t i = 0; i < db->ecreds->length; i++)
    pdb_encode_credential(db, db->ecreds->arr[i]->val.ptr, &pool);

  /* Save payload */
  {
    bool result;
    wchar_t *key = pdb_getkey(db);

    result = encrypt_to_file(db->path, db->iterations, key, pool.size, pool.base);

    sfree(wcslen(key) * sizeof(wchar_t), key);
    mpool_free(&pool);

    return result;
  }
}

void pdb_free(PDB_DATABASE *db) {
  free(db->path);
  sfree(db->keysz, db->key);

  for(size_t i = 0; i < db->ecreds->length; i++)
    free_credential(db->ecreds->arr[i]->val.ptr);

  qc_al_free(db->ecreds);
  sfree(sizeof(PDB_DATABASE), db);
}

wchar_t *pdb_getkey(PDB_DATABASE const *db) {
  wchar_t *k = calloc(db->keysz + 1, sizeof(wchar_t));

  pdb_crypt(db, db->keysz, db->key, (uint8_t *)k);

  return k;
}

void pdb_set_data_text(PDB_DATABASE const *db, PDB_DATA *d, const wchar_t *value) {
  if(d->value)
    sfree(d->size, d->value);

  d->type = PDB_DATA_TYPE_TEXT;
  d->value = to_utf8(value);
  d->size = (strlen(d->value) + 1) * sizeof(char);
  
  pdb_crypt(db, d->size, d->value, d->value);
}

wchar_t *pdb_get_data_text(PDB_DATABASE const *db, PDB_DATA *d) {
  char *out = malloc(d->size);
  wchar_t *ret;

  pdb_crypt(db, d->size, d->value, (uint8_t *)out);
  ret = from_utf8(out);

  sfree(d->size, out);
  return ret;
}

void pdb_set_data_file(PDB_DATABASE const *db, PDB_DATA *d, const wchar_t *path) {
  FILE *f;
  char *spath = to_multibyte(path);
  long sz;

  if(d->value)
    sfree(d->size, d->value);

  d->type = PDB_DATA_TYPE_FILE;

  f = fopen(spath, "rb");

  fseek(f, 0, SEEK_END);
  sz = ftell(f);
  rewind(f);

  d->size = sz;
  d->value = malloc(sz);
  fread(d->value, sz, 1, f);
  fclose(f);
  
  pdb_crypt(db, d->size, d->value, d->value);
  free(spath);
}

void pdb_get_data_file(PDB_DATABASE const *db, PDB_DATA *d, const wchar_t *path) {
  FILE *f;
  char *spath = to_multibyte(path);
  uint8_t *out = malloc(d->size);

  pdb_crypt(db, d->size, d->value, out);

  f = fopen(spath, "wb");
  fwrite(out, d->size, 1, f);
  fclose(f);

  sfree(d->size, out);
}

void pdb_remove_credential(PDB_DATABASE const *db, PDB_CREDENTIAL *c) {
  QC_AL_NODE *n = NULL;
  
  for(size_t i = 0; i < db->ecreds->length; i++)
    if(db->ecreds->arr[i]->val.ptr == c) {
      n = (QC_AL_NODE *)db->ecreds->arr[i];
      break;
    }

  free_credential(c);
  qc_al_remove(db->ecreds, n);
}
