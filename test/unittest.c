/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#define QUNIT_TESTFILE_VERSION "2.1"

#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <endian.h>
#include <gcrypt.h>
#include "qunit.h"
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "../encrypt.h"
#include "../pdb.h"
#include "../scramble.h"
#include "../util.h"
#include "../mpool.h"
#include "../helper.h"

static wchar_t *get_user() {
  struct passwd *pwd = getpwuid(getuid());
  
  return from_multibyte(pwd->pw_name);
}

static wchar_t *get_homedir() {
  struct passwd *pwd = getpwuid(getuid());
  
  return from_multibyte(pwd->pw_dir);
}

QUNIT_TEST(charset_utf8) {
  const char *txt_u8 = u8"Hello, wørld!";
  const wchar_t *txt_wc = L"Hello, wørld!";
  char *txt_wc2u8 = to_utf8(txt_wc);
  wchar_t *txt_u82wc = from_utf8(txt_u8);

  qunit_assert(wcscmp(txt_wc, txt_u82wc) == 0);
  qunit_assert(strcmp(txt_u8, txt_wc2u8) == 0);

  free(txt_wc2u8);
  free(txt_u82wc);
}

QUNIT_TEST(memory_pool) {
  MPOOL pool;
  #define DATASIZE kibibytes(700)
  #define VAL16 0xAABB
  #define VAL32 0xCCDDEEFF
  #define VAL64 0x0011223344556677
  uint8_t *data = malloc(DATASIZE);
  const char *strdata = "ciao";
  size_t offset = 0;

  gcry_randomize(data, DATASIZE, GCRY_WEAK_RANDOM);
  mpool_init(&pool);

  mpool_write_u8(&pool, 0xFF);
  mpool_write_u16(&pool, VAL16);
  mpool_write_u32(&pool, VAL32);
  mpool_write_u64(&pool, VAL64);
  mpool_write_utf8(&pool, strdata);
  mpool_write_block(&pool, data, DATASIZE);

  qunit_assert(*(uint8_t*)pool.base == 0xFF); offset += sizeof(uint8_t);
  qunit_assert(*(uint16_t*)(pool.base + offset) == htobe16(VAL16)); offset += sizeof(uint16_t);
  qunit_assert(*(uint32_t*)(pool.base + offset) == htobe32(VAL32)); offset += sizeof(uint32_t);
  qunit_assert(*(uint64_t*)(pool.base + offset) == htobe64(VAL64)); offset += sizeof(uint64_t);
  qunit_assert(*(uint16_t*)(pool.base + offset) == htobe16(strlen(strdata))); offset += sizeof(uint16_t);
  qunit_assert(!strncmp((char *)pool.base + offset, strdata, strlen(strdata))); offset += sizeof(char) * strlen(strdata);
  qunit_assert(!memcmp((uint8_t *)pool.base + offset, data, DATASIZE));

  mpool_free(&pool);
  free(data);
  #undef VAL64
  #undef VAL32
  #undef VAL16
  #undef DATASIZE
}

QUNIT_TEST(tilde_expand) {
  const wchar_t *s1 = L"~/something"; /* don't change */
  wchar_t *user = get_user(), *homedir = get_homedir();
  size_t s2_len = 1 + wcslen(user) + wcslen(s1);
  wchar_t *s2 = malloc(sizeof(wchar_t) * s2_len);
  wchar_t *o; /* output */
  wchar_t *e; /* expected results */
  size_t e_len = wcslen(homedir) + wcslen(s1);

  e = malloc(sizeof(wchar_t) * e_len);
  swprintf(s2, s2_len, L"~%ls%ls", user, &s1[1]);
  swprintf(e, e_len, L"%ls%ls", homedir, &s1[1]);

  o = expand_tilde(s1);
  qunit_assert(wcscmp(e, o) == 0);
  free(o);

  o = expand_tilde(s2);
  qunit_assert(wcscmp(e, o) == 0);
  free(o);

  free(e);
  free(s2);
  free(user);
  free(homedir);
}

QUNIT_TEST(in_memory_encryption) {
  #define DATALEN 2049
  const wchar_t *key = L"My secret key";
  uint8_t plain[DATALEN];
  uint8_t out[DATALEN];
  uint8_t dout[DATALEN]; /* Decrypted output */
  uint8_t ctr[ENCRYPT_BLOCKSIZE];
  uint8_t ctr2[ENCRYPT_BLOCKSIZE];
  uint8_t salt[ENCRYPT_SALTSIZE];
  size_t keysz = wcslen(key) * sizeof(wchar_t);

  gcry_randomize(plain, DATALEN, GCRY_WEAK_RANDOM);
  gcry_create_nonce(ctr, ENCRYPT_BLOCKSIZE);
  gcry_create_nonce(salt, ENCRYPT_SALTSIZE);
  memcpy(ctr2, ctr, ENCRYPT_BLOCKSIZE);

  crypt(keysz, (const uint8_t *)key, salt, ctr, DATALEN, (const uint8_t *)plain, out, ENCRYPT_KEYHASH_ITERATIONS);
  crypt(keysz, (const uint8_t *)key, salt, ctr2, DATALEN, out, dout, ENCRYPT_KEYHASH_ITERATIONS);

  qunit_assert(memcmp(plain, dout, DATALEN) == 0);

  #undef DATALEN
}

QUNIT_TEST(pdb_key_encryption) {
  const wchar_t *key = L"My secret key";
  PDB_DATABASE *db = pdb_new(L"/random/db.pdb", key);
  wchar_t *k = NULL;

  k = pdb_getkey(db);
  qunit_assert(wcscmp(key, k) == 0);

  free(k);
  pdb_free(db);
}

QUNIT_TEST(scramble) {
  const char *a = "This test must be scrambled";
  size_t len = strlen(a);
  char *b = malloc(len * sizeof(char));
  int res = strncmp(scramble(len, b, SCRAMBLE_DEFAULT_ITERATIONS), a, len);

  free(b);

  qunit_assert(res != 0);
}

QUNIT_TEST(pbkdf2_hmac_sha256) {
  #define OUTLEN 128
  const char *key = "This is a secret test key to hash.";
  uint8_t salt[ENCRYPT_SALTSIZE];
  uint8_t out1[OUTLEN];
  uint8_t out2[OUTLEN];

  gcry_create_nonce(salt, ENCRYPT_SALTSIZE);
  encrypt_derive_key(strlen(key), key, salt, OUTLEN, out1, ENCRYPT_KEYHASH_ITERATIONS);
  encrypt_derive_key(strlen(key), key, salt, OUTLEN, out2, ENCRYPT_KEYHASH_ITERATIONS);

  qunit_assert_vector_equals(out1, out2, OUTLEN);

  #undef OUTLEN
}

QUNIT_TEST(file_encryption) {
  #define DATALEN 1068 /* Just a random value */
  #define ITERATIONS 1024
  const wchar_t *key = L"My secret key for file encryption";
  const char *spath = tmpnam(NULL);
  wchar_t *path = from_multibyte(spath);
  uint8_t *data = malloc(DATALEN);
  uint32_t out_iter;
  size_t out_sz;
  uint8_t *out_data = NULL;

  gcry_randomize(data, DATALEN, GCRY_WEAK_RANDOM);

  qunit_assert(encrypt_to_file(path, ITERATIONS, key, DATALEN, data));
  
  out_data = decrypt_from_file(path, key, &out_iter, &out_sz);
  unlink(spath); /* Clean temp file */

  qunit_assert(out_data != NULL);
  qunit_assert(out_iter == ITERATIONS);
  qunit_assert(out_sz == DATALEN);
  qunit_assert(memcmp(out_data, data, DATALEN) == 0);

  free(out_data);
  free(data);
  free(path);

  #undef ITERATIONS
  #undef DATALEN
}

QUNIT_TEST(database_save) {
  #define NCREDS 3
  const wchar_t *key = L"My secret key for file encryption";
  const char *spath = tmpnam(NULL);
  wchar_t *path = from_multibyte(spath);
  static const wchar_t *c_titles[] = {
    L"Credential 1",
    L"Credential 2",
    L"The credential 3"
  };
  static const wchar_t *c_urls[] = {
    L"https://myurl1.com",
    L"https://mygrl2.com",
    L"https://meurl4.com"
  };
  static const wchar_t *c_tags[] = {
    L"one, two, three",
    L"",
    L"a, b, c"
  };
  static const wchar_t *c_usernames[] = {
    L"one",
    L"two",
    L"three"
  };
  static const wchar_t *c_passwords[] = {
    L"secret1",
    L"thesecret2",
    L"boo-magic"
  };
  static const wchar_t *c_data_names[] = {
    L"data1",
    L"data2",
    L"something something data"
  };
  static const wchar_t *c_data_values[] = {
    L"content 1",
    L"content 2",
    L"*something something content"
  };
  PDB_DATABASE *pdb = pdb_new(path, key);
  PDB_CREDENTIAL *c;

  /* Populate */
  for(size_t i = 0; i < NCREDS; i++) {
    PDB_DATA *d;
    c = pdb_new_credential(pdb, c_titles[i]);

    pdb_set_credential_username(pdb, c, c_usernames[i]);
    pdb_set_credential_password(pdb, c, c_passwords[i]);
    pdb_set_credential_url(pdb, c, c_urls[i]);
    pdb_set_credential_tags(pdb, c, c_tags[i]);

    d = pdb_new_data(pdb, c, c_data_names[i], PDB_DATA_TYPE_TEXT);
    pdb_set_data_text(pdb, d, c_data_values[i]);
  }

  pdb_save(pdb);
  pdb_free(pdb);
  pdb = pdb_load(path, key);
  unlink(spath);
  free(path);

  qunit_assert(pdb != NULL);
  qunit_assert(pdb->ecreds->length == NCREDS);

  /* Check */
  for(size_t i = 0; i < pdb->ecreds->length; i++) {
    wchar_t *val;
    size_t j; /* Test data credential index */
    PDB_DATA *d;
    wchar_t *dtext;

    c = pdb->ecreds->arr[i]->val.ptr;

    for(j = 0; j < NCREDS; j++)
      if(wcscmp(c->title.ptr, c_titles[j]) == 0)
        break;

    /* Make sure we found the right credential index inside our test data */
    qunit_assert(j < NCREDS);

    #define TESTVAL(name, var) do {\
      val = pdb_get_credential_##name(pdb, c);\
      qunit_assert(wcscmp(val, var[j]) == 0);\
      free(val);\
    } while(0)

    TESTVAL(username, c_usernames);
    TESTVAL(password, c_passwords);
    TESTVAL(url, c_urls);
    TESTVAL(tags, c_tags);
    #undef TESTVAL

    /* Check data */
    qunit_assert(c->data->length == 1);
    d = c->data->arr[0]->val.ptr;
    dtext = pdb_get_data_text(pdb, d);
    qunit_assert(wcscmp(d->name.ptr, c_data_names[j]) == 0);
    qunit_assert(wcscmp(dtext, c_data_values[j]) == 0);
    
    free(dtext);
  }

  pdb_free(pdb);
  #undef NCREDS
}

QUNIT_TEST(remove) {
  #define NCREDS 3
  const wchar_t *key = L"My secret key for file encryption";
  static const wchar_t *c_titles[] = {
    L"Credential 1",
    L"Credential 2",
    L"The credential 3"
  };
  PDB_DATABASE *pdb = pdb_new(L"/tmp/some-unused-test-name.db", key);
  PDB_CREDENTIAL *c;

  /* Populate */
  for(size_t i = 0; i < NCREDS; i++)
    c = pdb_new_credential(pdb, c_titles[i]);

  /* Remove credential */
  pdb_remove_credential(pdb, c);

  /* Check */
  for(size_t i = 0; i < pdb->ecreds->length; i++) {
    c = pdb->ecreds->arr[i]->val.ptr;

    qunit_assert(wcscmp(c->title.ptr, c_titles[NCREDS-1]) != 0);
  }

  pdb_free(pdb);
}

QUNIT_TEST(ws_tolower) {
  wchar_t *in = L"This is a Test STRING.";
  wchar_t *expected = L"this is a test string.";
  wchar_t *out = ws_tolower(in);

  qunit_assert(wcscmp(out, expected) == 0);
  free(out); /* Of course this doesn't get freed on failure, but really: who cares? */
}

QUNIT_TEST(ws_isearch) {
  wchar_t *one = L"Something something";
  wchar_t *two = L"something something";
  wchar_t *tgt = L"Something";

  qunit_assert(ws_isearch(one, tgt));
  qunit_assert(!ws_isearch(two, tgt));
}

int main(int argc, char **argv) {
  QUNIT_TESTCASE tcase;

  setlocale(LC_ALL, "");
  qunit_print_header();

  qunit_tcase_init(&tcase);
  qunit_tcase_add_reporter(&tcase, qunit_reporter_console);

  qunit_tcase_add(&tcase, ws_tolower);
  qunit_tcase_add(&tcase, ws_isearch);
  qunit_tcase_add(&tcase, charset_utf8);
  qunit_tcase_add(&tcase, memory_pool);
  qunit_tcase_add(&tcase, tilde_expand);
  qunit_tcase_add(&tcase, in_memory_encryption);
  qunit_tcase_add(&tcase, pdb_key_encryption);
  qunit_tcase_add(&tcase, scramble);
  qunit_tcase_add(&tcase, remove);
  qunit_tcase_add(&tcase, pbkdf2_hmac_sha256);
  qunit_tcase_add(&tcase, file_encryption);
  qunit_tcase_add(&tcase, database_save);

  {
    int result = (int)qunit_tcase_run(&tcase);

    return result;
  }
}
